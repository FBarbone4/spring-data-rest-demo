# Spring Data Rest
Progetto che ha lo scopo di affinare le capacità di utilizzo di **SPRING DATA REST** in modo da creare servizi rest che utilizzino la tecnologia [HATEOAS](https://it.wikipedia.org/wiki/HATEOAS). 
L'architettura di base del progetto è quella del **DDD** (Domain Driven Design) la cui implementazione spring è fornita proprio da Spring Data Rest inoltre la struttura del progetto è stata pensata come un multiple maven module in modo da aggiungere ulteriori conoscenze.
---
### Riferimenti Spring Data rest
[Spring doc.](https://spring.io/projects/spring-data-rest#learn)
